package service

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/intouchKey/golang-jwt/src/repository"
	"golang.org/x/crypto/bcrypt"
)

type LoginService struct {
	repo repository.UserRepository
}

func NewLoginService(repo repository.UserRepository) LoginService {
	return LoginService{repo: repo}
}

func (loginService *LoginService) LoginUser(email string, password string, ctx *gin.Context) bool {
	user, err := loginService.repo.FindByEmail(email, ctx);
	if err != nil {
		return false
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return false
	}

	return true
}

