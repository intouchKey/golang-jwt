package service

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/intouchKey/golang-jwt/src/repository"
)

type RegisterService struct {
	repo repository.UserRepository
}

func NewRegisterService(repo repository.UserRepository) RegisterService {
	return RegisterService{repo}
}

func (r *RegisterService) RegisterUser(email string, hashedPassword string, ctx *gin.Context) bool {
	if err := r.repo.Create(email, hashedPassword); err != nil {
		return false
	}

	return true
}
