package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/intouchKey/golang-jwt/src/dto"
	"gitlab.com/intouchKey/golang-jwt/src/service"
	"net/http"
)

type LoginController interface {
	Login(ctx *gin.Context)
}

type loginController struct {
	loginService service.LoginService
	jWtService   service.JWTService
}

func LoginHandler(loginService service.LoginService,
	jWtService service.JWTService) LoginController {
	return &loginController{
		loginService: loginService,
		jWtService:   jWtService,
	}
}

func (controller *loginController) Login(ctx *gin.Context) {
	var credential dto.LoginCredentials

	err := ctx.ShouldBind(&credential)
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"error": "Invalid JSON",
		})
		return
	}

	isUserAuthenticated := controller.loginService.LoginUser(credential.Email, credential.Password, ctx)
	if isUserAuthenticated {
		token := controller.jWtService.GenerateToken(credential.Email, true)
		ctx.JSON(http.StatusOK, gin.H{
			"token": token,
		})
		return
	}

	ctx.JSON(http.StatusUnauthorized, gin.H{
		"error": "Unathorized access",
	})
}