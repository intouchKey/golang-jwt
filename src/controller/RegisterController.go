package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/intouchKey/golang-jwt/src/dto"
	"gitlab.com/intouchKey/golang-jwt/src/service"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

type RegisterController interface {
	Register(ctx *gin.Context)
}

type registerController struct {
	jWtService   service.JWTService
	registerService service.RegisterService
}

func RegisterHandler(registerService service.RegisterService,
	jWtService service.JWTService) RegisterController {
	return &registerController{
		jWtService:   jWtService,
		registerService: registerService,
	}
}

func (controller *registerController) Register(ctx *gin.Context) {
	var credential dto.LoginCredentials

	err := ctx.ShouldBind(&credential)
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"error": "Invalid JSON",
		})
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(credential.Password), 5)
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"error": "Bcrypt error",
		})
		return
	}

	success := controller.registerService.RegisterUser(credential.Email, string(hashedPassword), ctx)

	if !success {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"error": "email is taken",
		})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"info": "registered successfully",
	})
}