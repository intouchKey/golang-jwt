package repository

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/intouchKey/golang-jwt/src/models"
)

type Reader interface {
	FindByEmail(email string, ctx *gin.Context) (*models.User, error)
}

type Writer interface {
	Create(email string, hashedPassword string) error
}

type UserRepository interface {
	Reader
	Writer
}

type userRepo struct {}

func NewUserRepository() UserRepository {
	return &userRepo{}
}

func (u *userRepo) FindByEmail(email string, ctx *gin.Context) (*models.User, error) {
	var user models.User

	err := models.DB.Where(
		fmt.Sprintf("email = '%s'", email),
		ctx.Param("id")).First(&user,
	).Error;

	return &user, err
}

func (u *userRepo) Create(email string, hashedPassword string) error {
	user := models.User{
		Email:    email,
		Password: hashedPassword,
	}

	return models.DB.Create(&user).Error
}
