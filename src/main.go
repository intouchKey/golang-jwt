package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/intouchKey/golang-jwt/src/controller"
	"gitlab.com/intouchKey/golang-jwt/src/models"
	"gitlab.com/intouchKey/golang-jwt/src/repository"
	"gitlab.com/intouchKey/golang-jwt/src/service"
)

func main() {
	var userRepository = repository.NewUserRepository()
	var registerService = service.NewRegisterService(userRepository)
	var loginService = service.NewLoginService(userRepository)
	var jwtService = service.JWTAuthService()
	var loginController = controller.LoginHandler(loginService, jwtService)
	var registerController = controller.RegisterHandler(registerService, jwtService)

	server := gin.New()
	models.ConnectToDatabase()

	server.POST("/login", loginController.Login)
	server.POST("/register", registerController.Register)

	server.Run(":8082")
}