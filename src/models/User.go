package models

import (
	_ "gorm.io/gorm"
)

type User struct {
	ID     uint   `json:"id" gorm:"AUTO_INCREMENT"`
	Email  string `json:"email" gorm:"primary_key"`
	Password string `json:"password"`
}
